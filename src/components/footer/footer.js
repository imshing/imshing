import React from 'react'

import classes from './footer.module.scss'

const Footer = props => {
  const date = new Date()
  const year = date.getFullYear();
  return (
    <footer className={classes.el}>
      <div className={classes.social}>
        {
          props.social.map(item => {
            return (
              <a className={classes.socialItem} key={item.id} href={item.id === 'email' ? `mailto:${item.link}` : item.link} target="_blank" rel="noopener noreferrer">
                <div className={item.icon}></div>
              </a>
            )
          })
        }
      </div>
      <div className={classes.copyright}>&copy;{`${year} ${props.copyright}`}</div>
    </footer>
  )
}

export default Footer