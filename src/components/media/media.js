import React from 'react'
import Swiper from 'react-id-swiper'

import classes from './media.module.scss'

const Media = props => {
  const swiperParams = {
    allowTouchMove: false,
    loop: true,
    autoplay: {
      delay: 2500
    }
  }

  return (
    <div className={classes.el}>
      {
        props.media.length ? (
          <Swiper {...swiperParams}>
            {
              props.media.map(item => {
                return (
                  <div key={item.id}>
                    <img src={item.src} alt={item.alt} />
                  </div>
                )
              })
            }
          </Swiper>
        ) : (
          <img src={props.media.src} alt={props.media.alt} />
        )
      }
    </div>
  )
}

export default Media