import React from 'react'

import classes from './header.module.scss'

const Header = props => {
  return (
    <header className={classes.el}>
      <img src={props.logo} alt="Logo" />
    </header>
  )
}

export default Header