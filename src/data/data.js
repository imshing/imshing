//* Header Section
import logo from '../assets/images/logo.png'

//* About Me Section
import profilePic from '../assets/images/profile-image.jpg'
import resume from '../../static/docs/resume.pdf'

//* LandorAndFitch
import landorandfitch from '../assets/images/landorandfitch.jpg'

//* ITC mall
import itcmall from '../assets/images/itcmall.jpg'

//* BIRD Meditation
import birdMediation from '../assets/images/bird-meditation.jpg'

//* K11
import k11MuseaImg from '../assets/images/k11-musea.jpg'
import klub11Img from '../assets/images/klub-11.jpg'
import k11ArtImg from '../assets/images/k11-artmall.jpg'

//* Front End Project Workflow
import projWfwImg from '../assets/images/project-workflow.png'

//* Rosewood
import rosewoodImg from '../assets/images/rosewood.jpg'

//* CEM Macau
import cemImg from '../assets/images/cem-macau.jpg'

const content = {
  page: {
    title: 'im.shing | portfolio website | front-end developer',
    canonical: 'https://imshing.com',
    themeColor: '#d9dde0',
    description: 'This is Jacky Lo. A Front-End Developer located in Hong Kong. Welcome to my portfolio website and checkout my project. Nice to meet you!',
    author: 'Jacky Lo',
    keywords: 'im.shing, imshing, Lo Hao Cheng Jacky, 羅浩誠, Jacky Lo, imshing portfolio, @im.shing, hclojacky, lohaocheng, jackylo.hc@gmail.com'
  },
  header: {
    logo: logo
  },
  contents: [
    {
      id: 'intro',
      type: 'intro',
      data:  {
        media: {
          src: profilePic,
          alt: 'Profile'
        },
        title: 'About Me',
        name: 'Lo Hao Cheng Jacky | 羅浩誠',
        copy: '<p>Graduate student from HKPolyU Interactive Media. Interested in web development, interaction design and user experience design. Currently work at Hang Lung Properties Company as Analyst Programmer<br/><br/>Amateur photographer for more than 8 years. Recent year start to take film and Polaroid photography as it\'s unique and irreplaceable characteristics.<br/><br/>Travel and sports lover. Deep in love with solo travelling since 2015 Taiwan trip. Exploring the world is one of my life mission. Running since 2016',
        download_resume: {
          href: resume,
          text: 'Download Resume'
        }
      },
    },
    {
      id: 'landorandfitch',
      type: 'project',
      data: {
        media:{
          src: landorandfitch,
          alt: 'Landor And Fitch Preview'
        },
        title: 'Landor and Fitch Website',
        skills: 'React.js | Next.js | GSAP',
        copy: '<p>Last project in Fitch. This is a one-page website using Next.js for development. Including number counting animation, smooth page scrolling etc.</p>',
        links: [
          {
            id: 'landorandfitch-link',
            href: 'https://landorandfitch.com/',
            text: 'Landor and Fitch'
          }
        ]
      }
    },
    {
      id: 'itcmall',
      type: 'project',
      data: {
        media:{
          src: itcmall,
          alt: 'ITC Mall Website Preview'
        },
        title: 'ITC Mall Website',
        skills: 'React.js | Next.js | GSAP',
        copy: '<p>Project working in Fitch, first time for using Next.js on corporate website. Learning Next.js from their official website and kickstart the development. Integrated with Umbraco CMS that work with Back End Developer.</p>',
        links: [
          {
            id: 'itcmall-link',
            href: 'http://itcmall.com.cn/zh-cn',
            text: 'ITC Mall'
          }
        ]
      }
    },
    {
      id: 'bird-meditation',
      type: 'project',
      data: {
        media:{
          src: birdMediation,
          alt: 'BIRD Meditation Website Preview'
        },
        title: 'BIRD Meditation Website',
        skills: 'Gulp | Pug | SCSS | Babel | Git',
        copy: '<p>Project working in Fitch, developing new website for BIRD Meditation.</p>',
        links: [
          {
            id: 'bird-meditation-link',
            href: 'https://birdmeditation.com/',
            text: 'BIRD Meditation'
          }
        ]
      }
    },
    {
      id: 'k11',
      type: 'project',
      data: {
        media: [
          {
            id: 'k11-musea-image',
            src: k11MuseaImg,
            alt: 'K11 Musea Website Screen shot'
          },
          {
            id: 'k11-artmall-image',
            src: k11ArtImg,
            alt: 'K11 Art Mall Website Screen shot'
          },
          {
            id: 'klub-11-image',
            src: klub11Img,
            alt: 'Klub 11 Website Screen shot'
          },
        ],
        title: 'K11 Musea Website',
        skills: 'Gulp | Pug | SCSS | ES6',
        copy: '<p>Main developer for the project in Fitch Company. Created more than 20 pages of templates and components for integration into Umbraco CMS. Based on my self project "Front end Workflow Template" to kickstart this project.</p>',
        links: [
          {
            id: 'k11-musea-link',
            href: 'https://www.k11musea.com/',
            text: 'K11 Musea'
          },
          {
            id: 'k11-artmall-link',
            href: 'https://hk.k11.com/',
            text: 'K11 Art'
          },
          {
            id: 'klub-11-link',
            href: 'https://klub-11.com/',
            text: 'Klub 11'
          }
        ]
      }
    },
    {
      id: 'project-workflow',
      type: 'project',
      data: {
        media:{
          src: projWfwImg,
          alt: 'Front end Project Workflow graphic'
        },
        title: 'Front-end Project Workflow',
        skills: 'Gulp | Pug | SCSS | Babel | Git',
        copy: '<p>Personal Project to simplify my development time. By using Gulp.js, Pug.js, SCSS, and Babel those modern web tools to fasten the project workflow and build a performance website.</p>',
        links: [
          {
            id: 'project-workflow-link',
            href: 'https://gitlab.com/hclojacky/frontend-starter-template',
            text: 'Front-end Project Workflow'
          }
        ]
      }
    },
    {
      id: 'rosewood',
      type: 'project',
      data: {
        media: {
          src: rosewoodImg,
          alt: 'Rosewood website screen shot'
        },
        title: 'Rosewood Luxury Hotels Website',
        skills: 'XSLT | LESS | Gulp | jQuery | Sitecore6',
        copy: '<p>Project when working in Isobar Hong Kong. The main responsibility is to provide support and maintain for the clients. Developing a new front-end template from design and integrate with Sitecore CMS.</p>',
        links: [
          {
            id: 'rosewood-link',
            href: 'https://www.rosewoodhotels.com/en/default',
            text: 'Rosewood Hotel'
          }
        ]
      }
    },
    {
      id: 'cem',
      type: 'project',
      data: {
        media: {
          src: cemImg,
          alt: 'CEM Macau Website Screen shot'
        },
        title: 'CEM Macau Website',
        skills: 'Jade | SCSS | jQuery | Gulp | Git',
        copy: '<p>Project when working in Firmstuido Ltd. Team up with seniors to build an HTML template using Jade and Gulp, with the power of Git that can allow us to code separately and merge it afterward.</p>',
        links: [
          {
            id: 'cem-macau-link',
            href: 'https://www.cem-macau.com/zh/',
            text: 'CEM Macau'
          }
        ]
      }
    }
  ],
  footer: {
    social: [
      {
        id: 'email',
        icon: 'icon-email',
        link: 'jackylo.hc@gmail.com'
      },
      {
        id: 'instagram',
        icon: 'icon-instagram',
        link: 'https://www.instagram.com/imshing.dp/'
      },
      {
        id: 'linkedin',
        icon: 'icon-linkedin',
        link: 'https://www.linkedin.com/in/imshing'
      },
      {
        id: 'gitlab',
        icon: 'icon-gitlab',
        link: 'https://gitlab.com/hclojacky'
      },
      {
        id: 'unsplash',
        icon: 'icon-unsplash',
        link: 'https://unsplash.com/@hclojacky'
      }
    ],
    copyright: 'im.shing'
  }
}

export default content