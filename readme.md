## [imshing.com](https://imshing.com) Portfolio Website

Personal Portfolio Website using [Gatsby.js](https://www.gatsbyjs.org/)

To Start the development in your local machine, you have to install [Node.js](https://nodejs.org/en/) with version `12.16.1` or above.

Clone this git repository to your computer:
```
git clone https://gitlab.com/hclojacky/imshing.git
```

Go to directory
```
cd imshing
```

Install Node Packages
```
npm install
```

Start the development
```
npm start
```
